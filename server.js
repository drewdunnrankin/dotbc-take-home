const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();

var app = express();

// connect to mongoDB instance
mongoose.connect(process.env.MONGODB_URI);
mongoose.set('debug', true);

// set up mongoose models
const ArtistModel = mongoose.model("artist", mongoose.Schema({
  artistId: String,
  name: String,
  spotifyId: String,
  genres: [String],
}));

const LabelModel = mongoose.model("label", {
  labelId: String,
  name: String,
  distributor: String,
  region: String
});

const ReleaseModel = mongoose.model("release", {
  title: String,
  releaseDate: String,
  trackCount: Number,
  upc: String,
  artists: [String],
  label: String,
  type: String
})

// set up body parsing middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// home route

app.get("/", async (request, response) => {
  try {
    response.send('Welcome to this mini API!');
  } catch (error) {
    response.status(500).send(error);
  }
});

// ARTISTS

// multiple name or ID search: default is to list all artists
app.get("/artists", async (request, response) => {
  try {
    if (request.query.q) {
      var artists = await ArtistModel.find({ $text: { $search: request.query.q }}, { _id: 0 }).exec();
      var artistIdList = artists.map(artist => artist.artistId);
      var releases = await ReleaseModel.find({ artists: { $in: artistIdList }}, { _id: 0 });
      var labelList = releases.map(release => release.label);
      var labels = await LabelModel.find({ labelId: { $in: labelList }}, { _id: 0 })
      var result = { artists: artists, releases: releases, labels: labels}
    } else {
      var result = await ArtistModel.find({}, { _id: 0 }).exec();
    }
    response.send(result);
  } catch (error) {
    response.status(500).send(error);
  }
});

// single name or ID search
app.get("/artists/:search", async (request, response) => {
  try {
    var artist = await ArtistModel.findOne({ $text: { $search: request.params.search } }, { _id: 0 });
    var releases = await ReleaseModel.find({ artists: artist.artistId }, { _id: 0 });
    var labelList = releases.map(release => release.label);
    var labels = await LabelModel.find({ labelId: { $in: labelList }}, { _id: 0 });
    var result = { artist: artist, releases: releases, labels: labels }
    
    response.send(result); 
  } catch(error) {
    response.status(500).send(error);
  }
});

// RELEASES

// list all releases
app.get("/releases", async (request, response) => {
  try {
    var result = await ReleaseModel.find({}, { _id: 0 }).exec();
    response.send(result);
  } catch(error) {
    response.status(500).send(error);
  }
})

// release type search 
app.get("/releases/:type", async (request, response) => {
  try {
    var releases = await ReleaseModel.find({ type: request.params.type }, { _id: 0 });
    var artists = new Set(releases.flatMap(release => release.artists))
    var labels = new Set(releases.map(release => release.label))
    artists = await ArtistModel.find({ artistId: { $in: [...artists] }}, { _id: 0 });
    labels = await LabelModel.find({ labelId: { $in: [...labels] }}, { _id: 0 });
    var result = { releases: releases, artists: artists, labels: labels }
    response.send(result); 
  } catch(error) {
    response.status(500).send(error);
  }
});

// release UPC search
app.get("/releases/upc/:upc", async (request, response) => {
  try {
    var release = await ReleaseModel.findOne({ upc: request.params.upc }, { _id: 0 });
    var artists = await ArtistModel.find({ artistId: { $in: release.artists }}, { _id: 0 });
    var labels = await LabelModel.findOne({ labelId: release.label }, { _id: 0 });
    var result = { release: release, artists: artists, label: labels }
    response.send(result); 
  } catch(error) {
    response.status(500).send(error);
  }
});

// LABELS

// list all labels
app.get("/labels", async (request, response) => {
  try {
    var result = await LabelModel.find({}, { _id: 0 }).exec();
    response.send(result);
  } catch(error) {
    response.status(500).send(error);
  }
});

// label ID search
app.get("/labels/:id", async (request, response) => {
  try {
    var label = await LabelModel.findOne({ labelId: request.params.id }, { _id: 0 });
    var releases = await ReleaseModel.find({ label: label.labelId }, {_id: 0 })
    var artists = new Set(releases.flatMap(release => release.artists))
    artists = await ArtistModel.find({ artistId: { $in: [...artists] }}, { _id: 0 });
    var result = { label: label, releases: releases, artists: artists }
    response.send(result);
  } catch(error) {
    response.status(500).send(error);
  } 
});

// set up server
var server = app.listen(process.env.PORT || 3000, function() {
  var port = server.address().port;
  console.log("App now running on port", port);
});